<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>s2: Repetition Control Structures and Array Manipulation</title>
	</head>
	<body>

		<h1>Repetition Control Structures</h1>

		<?php whileLoop(); ?>
		<?php doWhileLoop(); ?>
		<?php forLoop(); ?>
		<?php modifiedForLoop(); ?>

		<h2>Associative Array</h2>
		<?php echo $gradePeriods -> firstGrading; ?>

		<ul>
			<?php forEach($gradePeriods as $period =>$grade) { ?>

				<li>Grade in <?= $period ?> is <?= $grade ?></li>
				<?php } ?>


			
		</ul>

		<h2>2-Dimensional Array</h2>
		<?php echo $heroes[2] [1] ?>

		<ul>
			<?php 
				foreach($heroes as $team) {
					forEach($team as $member) {
						?>

						<li><?php echo $member ?></li>
						<?php
					}
				}
			?>
		</ul>

		<h2>Sorting</h2>
		<pre>
			<?php print_r($sortedBrands); ?>
		</pre>

		<h2>Reverse Sorting</h2>
		<pre>
			<?php print_r($reverseSortedBrands); ?>
		</pre>
		
		<h2>Array Mutations (Append)</h2>

		<h3>push</h3>
		<?php array_push($computerBrands, 'Apple') ?>

		<pre>
			<?php print_r($computerBrands);?>
		</pre>

		<h3>unshift</h3>
		<?php array_unshift($computerBrands, 'Dell') ?>

		<pre>
			<?php print_r($computerBrands);?>
		</pre>

		<h2>Remove</h2>

		<?php array_pop($computerBrands); ?>
		<pre>
			<?php print_r($computerBrands);?>
		</pre>

		<?php array_shift($computerBrands); ?>
		<pre>
			<?php print_r($computerBrands);?>
		</pre>

		<h2>Count</h2>
		<pre>
			<?php echo count($computerBrands);?>
		</pre>

		<p>
			<?php echo searchBrand($computerBrands, 'HP')?>
		</p>

		<pre>
			<?php print_r($reverseSortedBrands); ?>
		</pre>


		<h1>Activity</h1>

		<h2>Divisible by 5</h2>
		<p><?php echo printNumbers(); ?></p>

		<h2>Array Manipulation</h2>
		
		<p><?php array_push($students, 'Primo') ?></p>

		<p><?php var_dump($students); ?></p>

		<p><?php echo count($students);?></p>

		<p><?php array_push($students, 'George') ?></p>

		<p><?php var_dump($students); ?></p>

		<p><?php echo count($students);?></p>

		<p><?php array_shift($students) ?></p>

		<p><?php var_dump($students); ?></p>

		<p><?php echo count($students);?></p>














	</body>
</html>