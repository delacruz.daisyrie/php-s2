<?php

// While Loop
function whileLoop() {
	$count = 5;

	while($count !== 0) {
		echo $count.'<br/>';
		$count--;
	}
}

// Do-While Loop
function doWhileLoop() {
	$count = 20;

	do {
		echo $count.'<br/>';
		$count--;
	} 
	while($count > 0);

}

// For Loop
function forLoop() {
	for($count = 0; $count <= 20; $count++) {
		echo $count.'<br/>';
	}
}

// continue and break statements

function modifiedForLoop() {
	for ($count = 0; $count <= 20; $count++) {
		if ($count % 2 === 0) {
			continue;
		}
		echo $count.'<br/>';
		if($count > 10) {
			break;
		}
	}
}

// Array Manipulation
$studentNumbers = array('2020-1923', '2020-1924', '2020-1925');

$newStudentNumbers = [
	'2022-1111', 
	'2022-1112', 
	'2022-1113'];


// Simple Arrays
$tasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];

// Associative arrays
$gradePeriods = [
	'firstGrading' => 98.5,
	'secondGrading' => 94.3,
	'thirdGrading' => 90,
	'fourthGrading' => 97
];

// Two-Dimensional Array
$heroes  = [
	['Iron Man', 'Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Jean', 'Grey'],
	['Batman', 'Superman', 'Wonder Woman']
];

// Array Iterative Method: foreach()

foreach($heroes as $hero) {
	echo $hero;
}

// Array Sorting
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

$sortedBrands = $computerBrands;
$reverseSortedBrands = $computerBrands;

sort($sortedBrands);
rsort($reverseSortedBrands);


// Other Array Functions
function searchBrand($brands, $brand) {
	return (in_array($brand, $brands)) ? "$brand is in the array." : "$brand is not in the array.";
}

$reversedGradePeriods = array_reverse($gradePeriods);


// Activity

function printNumbers() {
	for ($count = 0; $count <= 100; $count++) {
		if($count % 5 ===0) {
			echo $count. ",";
		}
		if($count === 100) {
			break;
		}
	}
}

$students = [];







?>